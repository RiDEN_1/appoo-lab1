public class ICEngine extends Engine {
    private String fuelType;
    public ICEngine(int power, int torque, String fuelType) {
        super(power, torque);
        this.fuelType = fuelType;
    }

    public String getFuelType() {
        return fuelType;
    }
}
