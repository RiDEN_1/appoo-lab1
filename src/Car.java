import static java.lang.Math.pow;

public class Car extends Vehicle {
    private boolean isAWD;
    public Car(int mass, Engine engine, String name, boolean isAWD) {
        super(mass, engine, name);
        this.isAWD=isAWD;
    }
    @Override
    public void dragrace() {
        double time = pow(this.getMass() / this.getEngine().getPower(), 1.0/3.0) * 5.825 + (this.isAWD?0:1);
        System.out.println(this.getName()+"  time  "+time);
    }

}