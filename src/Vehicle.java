public abstract class Vehicle {
    private String name;
    private int mass;
    private Engine engine;

    public int getMass() {
        return mass;
    }

    public String getName() {
        return name;
    }

    public Engine getEngine() {
        return engine;
    }

    public Vehicle (int mass, Engine engine, String name){
        this.mass = mass;
        this.engine = engine;
        this.name = name;
    }
    public abstract void dragrace();
}
