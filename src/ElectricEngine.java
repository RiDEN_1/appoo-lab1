public class ElectricEngine extends Engine {
    private int maxAmp;
    public ElectricEngine(int power, int torque, int maxAmp) {
        super(power, torque);
        this.maxAmp=maxAmp;
    }

    public int getMaxAmp() {
        return maxAmp;
    }
}
