public abstract class Engine {
    private int power;
    private int torque;

    public int getPower() {
        return power;
    }
    public int getTorque() {
        return torque;
    }
    public Engine(int power, int torque) {
        this.power = power;
        this.torque = torque;
    }
}