import java.util.ArrayList;
import java.util.Scanner;

public class Database {
    static public ArrayList<Vehicle> vehicles = new ArrayList<>();
    public static void prompt(){
        Scanner in = new Scanner(System.in);
        System.out.println("enter vehicle name");
        String newName = in.nextLine();
        System.out.println("mass?");
        int newMass=Integer.parseInt(in.nextLine());
        System.out.println("power?");
        int newPower=Integer.parseInt(in.nextLine());
        System.out.println("torque?");
        int newTorque=Integer.parseInt(in.nextLine());
        System.out.println("Car or Moto?");
        String newType = in.nextLine().toLowerCase();
        switch (newType){
            case "car" : {
                System.out.println("AWD?");
                boolean isNewAWD = in.nextBoolean();
                in.nextLine();
                System.out.println("IC or Electric?");
                String newEngType = in.nextLine().toLowerCase();
                switch (newEngType){
                    case "ic":
                        System.out.println("fueltype?");
                        String newFueltype = in.nextLine().toLowerCase();
                        Car newICVehicle = new Car(newMass, new ICEngine(newPower, newTorque, newFueltype), newName, isNewAWD);
                        vehicles.add(newICVehicle);
                        break;
                    case "electric":
                        System.out.println("maxAmp?");
                        int newAmp = Integer.parseInt(in.nextLine().toLowerCase());
                        Car newElVehicle = new Car(newMass, new ElectricEngine(newPower, newTorque, newAmp), newName, isNewAWD);
                        vehicles.add(newElVehicle);
                        break;
                }
                break;
            }
            case "moto" :{
                System.out.println("ARB?");
                boolean isNewARB = in.nextBoolean();
                in.nextLine();
                System.out.println("IC or Electric?");
                String newEngType = in.nextLine().toLowerCase();
                switch (newEngType){
                    case "ic":
                        System.out.println("fueltype?");
                        String newFueltype = in.nextLine().toLowerCase();
                        Motorcycle newICVehicle = new Motorcycle(newMass, new ICEngine(newPower, newTorque, newFueltype), newName, isNewARB);
                        vehicles.add(newICVehicle);
                        break;
                    case "electric":
                        System.out.println("maxAmp?");
                        int newAmp = Integer.parseInt(in.nextLine().toLowerCase());
                        Motorcycle newElVehicle = new Motorcycle(newMass, new ElectricEngine(newPower, newTorque, newAmp), newName, isNewARB);
                        vehicles.add(newElVehicle);
                        break;
                }
                break;
            }
        }
    }
}
