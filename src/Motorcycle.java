import static java.lang.Math.pow;

public class Motorcycle extends Vehicle{
    private boolean hasARbar;
    public Motorcycle(int mass, Engine engine, String name, boolean hasARbar) {
        super(mass, engine, name);
        this.hasARbar=hasARbar;
    }

    @Override
    public void dragrace() {
        double time = 5.825*pow(this.getMass()/this.getEngine().getPower(),1.0/3.0) + (this.hasARbar?0:1);
        System.out.println(this.getName()+"  time  "+time);
    }

}
